Aufgabe 1:
Die Grafik zeigt, dass ein kleiner Bereich den größten Einfluss auf die Klassifikation hat.
Insbesondere Guanin ist hier wichtig

Aufgabe 2:
Testfehlerrate sinkt mit Erhöhung von k anfangs deutlich, später kaum noch.
k=1: mean error 0.38484
k=3: mean error 0.362865
k=5: mean error 0.342698
k=7: mean error 0.318359
k=9: mean error 0.312796
k=11: mean error 0.317524
k=13: mean error 0.311405
k=15: mean error 0.30904
k=17: mean error 0.305841
k=19: mean error 0.301947
k=21: mean error 0.295688
k=23: mean error 0.305981
k=25: mean error 0.307371
