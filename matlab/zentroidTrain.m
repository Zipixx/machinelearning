function [weight, bias, mp, mn] = zentroidTrain(xTrain, yTrain)
  mp = mean(xTrain(:, yTrain > 0),2);
  mn = mean(xTrain(:, yTrain < 0),2);
  weight = mp - mn;
  bias = ((mn(1)^2 + mn(2)^2) - (mp(1)^2 + mp(2)^2)) / 2;
end