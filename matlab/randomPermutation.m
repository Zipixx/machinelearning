function [xTrain, yTrain, xTest, yTest] = randomPermutation(pos, neg)
  #cut both matrices in half after random permutation and generate y vector
  
  [dim, posSize] = size(pos);
  [dim, negSize] = size(neg);
  
  posCut = floor(posSize/2);
  negCut = floor(negSize/2);
  
  posPerm = randperm(posSize);
  posTrain = pos(:,posPerm(1:posCut));
  posTest = pos(:,posPerm(posCut+1:posSize));
  
  negPerm = randperm(negSize);
  negTrain = neg(:,negPerm(1:negCut));
  negTest = neg(:,negPerm(negCut+1:negSize));
  
  xTrain = [posTrain, negTrain];
  xTest = [posTest, negTest];
  
  yTrain = [ones(1,posCut) , -ones(1, negCut)];
  yTest = [ones(1, posSize - (posCut+1) + 1) , -ones(1, negSize - (negCut+1) + 1)];
  
end