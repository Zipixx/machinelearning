function nErrorTest = perceptronTest(XTestMat, YTestVec, weight, bias)
  
  [nDim nVecs] = size(XTestMat);
  nErrorTest=0;
  
  X = weight' * XTestMat + bias;
  X = X.*YTestVec;
  X(X(:,:)>=0)=0;
  X(X(:,:)<0)=1;
  nErrorTest=sum(X);

  
#seperator = @(x) weight' * x + bias;
#  for i = 1:nVecs
#    if seperator(XTestMat(:,i)) < 0
#      if YTestVec(i) == 1
#        nErrorTest += 1;
#      end
#    else
#      if YTestVec(i) == -1
#        nErrorTest += 1;
#      end
#    end
#  end
end