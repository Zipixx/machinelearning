
pos = reshape(fscanf(fopen("../data/pos.seq", 'r'), "%s"), 200, [])';
neg = reshape(fscanf(fopen("../data/neg.seq", 'r'), "%s"), 200, [])';

pa = double((pos == 'A')');
pt = double((pos == 'T')');
pg = double((pos == 'G')');
pc = double((pos == 'C')');

na = double((neg == 'A')');
nt = double((neg == 'T')');
ng = double((neg == 'G')');
nc = double((neg == 'C')');

posMat = [pa; pc; pg; pt];
negMat = [na; nc; ng; nt];

hold
iterations = 10;
for k=1:2:25
  d=zeros(1,iterations);
  for i=1:iterations
    [xTrain, yTrain, xTest, yTest] = randomPermutation(posMat, negMat);
    [dim nVecs] = size(xTest);
    
    d(i) = nearestNeighbor(xTrain, yTrain, xTest, yTest, k);
  end
  bar(k, mean(d/nVecs));
  printf("k=%d: mean error %d\n", k, mean(d/nVecs));
end






