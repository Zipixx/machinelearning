pos = loadSequenceFromFile("../data/pos.seq", 200);
neg = loadSequenceFromFile("../data/neg.seq", 200);


pa = double((pos == 'A')');
pt = double((pos == 'T')');
pg = double((pos == 'G')');
pc = double((pos == 'C')');

na = double((neg == 'A')');
nt = double((neg == 'T')');
ng = double((neg == 'G')');
nc = double((neg == 'C')');

posMat = [pa; pt; pg; pc];
negMat = [na; nt; ng; nc];

nTest = 10;
errors = zeros(nTest,1);
for i=1:nTest
  [xTrain, yTrain, xTest, yTest] = randomPermutation(posMat, negMat);
  [weight, bias, nUpdatesTrain] = perceptronTrain(xTrain, yTrain, false, 0.01);
  errors(i) = perceptronTest(xTest, yTest, weight, bias);
  #printf("error count %d\n", errors(i));
end
[dim nVecs] = size(xTest);
disp(errors');
printf("Error rate percentage: mean %d, std %d, min %d, max %d\n", mean(errors/nVecs), std(errors/nVecs), min(errors/nVecs), max(errors/nVecs));



