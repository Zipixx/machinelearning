function SeqMat = loadSequenceFromFile(path, seqLength)
  SeqMat=reshape(fileread(path), [], seqLength+1);
  SeqMat=SeqMat(:,1:seqLength);
end