nDims=2;
nVecs=100;
v = [0.9; 0.9];

center = [(1+v(1))/2; (1+v(1))/2];
XMat1=rand(nDims,nVecs);
XMat2=rand(nDims,nVecs) + repmat(v, 1, nVecs);

hold
#plot(center(1), center(2), '*', 'Color', [0 0 0])
seperator=@(x) center(2)-(x-center(1));
plot([0:2], seperator([0:2]), '-', 'Color', [0 1 0])

plot(XMat1(1,:), XMat1(2,:), '*r');
plot(XMat2(1,:), XMat2(2,:), '*b');

#plot first
index1 = convhull(XMat1(1,:),XMat1(2,:));
index2 = convhull(XMat2(1,:),XMat2(2,:));
plot(XMat1(1,:)(index1), XMat1(2,:)(index1), '-r');
plot(XMat2(1,:)(index2), XMat2(2,:)(index2), '-b');
if any(inpolygon(XMat1(1,:)(index1), XMat1(2,:)(index1), XMat2(1,:)(index2), XMat2(2,:)(index2)))
  printf("overlap in plotted example\n");
end
nOverlap = 0;
nTest = 10;
printf("Starting %d overlap-test runs\n", nTest);
for i=1:nTest 
  XMat1=rand(nDims,nVecs);
  XMat2=rand(nDims,nVecs) + repmat(v, 1, nVecs);
  index1 = convhull(XMat1(1,:),XMat1(2,:));
  index2 = convhull(XMat2(1,:),XMat2(2,:));
  overlap = inpolygon(XMat1(1,:)(index1), XMat1(2,:)(index1), XMat2(1,:)(index2), XMat2(2,:)(index2));
  if any(overlap)
    nOverlap = nOverlap + 1;
  end
end
printf("%d/%d overlaps\n", nOverlap, nTest);


