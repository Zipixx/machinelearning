#missing chars, dunno why
#pos = loadSequenceFromFile("../data/pos.seq", 200);
#neg = loadSequenceFromFile("../data/neg.seq", 200);

pos = reshape(fscanf(fopen("../data/pos.seq", 'r'), "%s"), 200, [])';
neg = reshape(fscanf(fopen("../data/neg.seq", 'r'), "%s"), 200, [])';

pa = double((pos == 'A')');
pt = double((pos == 'T')');
pg = double((pos == 'G')');
pc = double((pos == 'C')');

na = double((neg == 'A')');
nt = double((neg == 'T')');
ng = double((neg == 'G')');
nc = double((neg == 'C')');

posMat = [pa; pc; pg; pt];
negMat = [na; nc; ng; nt];
[xTrain, yTrain, xTest, yTest] = randomPermutation(posMat, negMat);

[weight, bias, mp, mn] = zentroidTrain(xTrain, yTrain);
nErrors = zentroidTest(xTest, yTest, mp, mn)

w = reshape(weight, 200, 4)';
#disp(w);
#disp(weight(1));
#disp(weight(201));
#disp(weight(401));
#disp(weight(601));
#disp(weight(2));
#disp(w(:,1));

imagesc(w);
colorbar;





