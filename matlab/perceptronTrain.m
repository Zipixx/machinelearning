function [WeightsVec, biasPair, nUpdatesTrain] = perceptronTrain(XTrainMat, YTrainVec, plot2d, stepW)
  if(nargin<3) return elseif(nargin==3) step=0.1; else step=stepW; end
  x=XTrainMat;
  y=YTrainVec;
  [dim, n]=size(x);
  #init weight
  w=zeros(dim, 1);
  b=0;
  k=0;
  r=max(sqrt(sum(x))); # max norm
  #r=norm(x)
  
  for iteration=1:1000
    err=0;
    min=Inf;
    for i=1:n
      #disp(y(i).*(w'*x(:,i)+b))
        #disp(size(x));
        #disp(size(y));
        #disp(size(w));
      cur = y(i)*(w'*x(:,i)+b);
      if(cur <= 0)
        #disp("error")
        #disp(w.+step.*y(i).*x(:,i))
        w = w + step.*y(i).*x(:,i);
        b = b + step*y(i)*r*r;
        k = k+1;
        err=1;
        if plot2d
          plot([0 -b/w(1)],[-b/w(2) 0], '-r');
        end
      else
        if (cur < min)
          min=cur;
        end
      end
    end
    if(err==0)
      printf("break at %d\n", iteration);
      gamma=min;
      break;
    end
  end  
  if err==0
    printf("errors: %d --- novikoff: %d\n", iteration, (4*r*r)/(gamma*gamma))
  end
  
  WeightsVec=w;
  biasPair=b;
  nUpdatesTrain=k;
end