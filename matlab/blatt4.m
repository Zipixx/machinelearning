nDims=2;
nVecs=40;
v = [0.9; 0.9];
nVecsStart = 40;
nVecsStep = 40;
nVecsMax = 320;
iterations = 10;

barData = zeros((nVecsMax-nVecsStart)/nVecsStep, 3, 2);
for nVecs=nVecs:40:nVecsMax
  d1=zeros(1,iterations);
  d2=zeros(1,iterations);
  d3=zeros(1,iterations);
  for i=1:iterations
    xPos=rand(nDims,nVecs);
    xNeg=rand(nDims,nVecs) + repmat(v, 1, nVecs);

    [xTrain, yTrain, xTest, yTest] = randomPermutation(xPos, xNeg);
    evalc('[pWeight, pBias, nUpdatesTrain] = perceptronTrain(xTrain, yTrain, false, 0.01);'); # surpress output
    [zWeight, zBias, mp, mn] = zentroidTrain(xTrain, yTrain);
    
    d1(i) = perceptronTest(xTest, yTest, pWeight, pBias);
    
    d2(i) = zentroidTest(xTest, yTest, mp, mn);
    
    d3(i) = nearestNeighbor(xTrain, yTrain, xTest, yTest, 1);
  end
  barData(nVecs/nVecsStep, 1, 1) = mean(d1/nVecs);
  barData(nVecs/nVecsStep, 2, 1) = mean(d2/nVecs);
  barData(nVecs/nVecsStep, 3, 1) = mean(d3/nVecs);
  #barData(nVecs/nVecsStep, 1, 2) = std(d1/nVecs);
  #barData(nVecs/nVecsStep, 2, 2) = std(d2/nVecs);
  #barData(nVecs/nVecsStep, 3, 2) = std(d3/nVecs);
  #printf("Error rate for nVecs=%d: d1: total %d, mean %d std %d , d2: total %d, mean %d std %d , d3: total %d, mean %d std %d\n"...
  #, nVecs, sum(d1), mean(d1/nVecs), std(d1/nVecs), sum(d2), mean(d2/nVecs), std(d2/nVecs), sum(d3), mean(d3/nVecs), std(d3/nVecs));
  
end

bar(barData(:,:,1));
#bar(barData(:,:,2));



