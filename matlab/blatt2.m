step=0.01;
nDims=2;
nVecs=50;
v = [0.9; 0.9];

#input

X1=rand(nDims,nVecs);
X2=rand(nDims,nVecs) + repmat(v, 1, nVecs);
X=[X1, X2];
#output class [-1,+1];
Y=[-ones(1,nVecs), ones(1,nVecs)];

hold
axis([0 2 0 2])

#plot([0:2], y([0:2]),'-b')

for i = 1:(2*nVecs)
  #if seperator(X(:,i)) < 0
  if i<=nVecs
    plot(X(1,i), X(2,i) ,'*', 'Color',[1 0 0])
  else
    plot(X(1,i), X(2,i) ,'*', 'Color',[0 0 1])
  end
end


[weight, bias, nUpdatesTrain] = perceptronTrain(X, Y, true,  step)

plot([0 -bias/weight(1)],[-bias/weight(2) 0], 'g--o');

iterations=10;
errors = zeros(1,iterations);
for i=1:iterations
  X1Test=rand(nDims,nVecs);
  X2Test=rand(nDims,nVecs) + repmat(v, 1, nVecs);
  
  #linear seperierbar?
  index1 = convhull(X1Test(1,:),X1Test(2,:));
  index2 = convhull(X2Test(1,:),X2Test(2,:));
  overlap = inpolygon(X1Test(1,:)(index1), X1Test(2,:)(index1), X2Test(1,:)(index2), X2Test(2,:)(index2));
  if any(overlap)
    i=i-1;
    disp("overlap found, generating new data set");
    continue
  end
  XTest=[X1Test, X2Test];
  YTest=[-ones(1,nVecs), ones(1,nVecs)];

  nErrorTest = perceptronTest(XTest, YTest, weight, bias);
  errors(i)=nErrorTest;
end
disp(errors)
printf("Error rate percentage: mean %d, std %d, min %d, max %d\n", mean(errors/(2*nVecs)), std(errors/(2*nVecs)), min(errors/(2*nVecs)), max(errors/(2*nVecs)));

