function nErrors = nearestNeighbor(xTrain, yTrain, xTest, yTest, k)
  [dim trainSize] = size(xTrain);
  [dim testSize] = size(xTest); 
  
  nErrors = 0;
  #dist = xTrain.^2 + xTest.^2 - 2.*xTrain.*xTest;
  %{
  for i=1:testSize
    minDistance = Inf;
    minLabel = 0;
    for j=1:trainSize
      dist = norm(xTrain(:,j) - xTest(:,i));
      if dist < minDistance
        minDistance = dist;
        minLabel = yTrain(j);
      end
    end
    if minLabel ~= yTest(i)
      nErrors += 1;
    end
  end
  %}
  
  #distance matrix
  xTrain = xTrain';
  dist = repmat(sum(xTrain.^2, 2), 1, testSize) + repmat(sum(xTest.^2), trainSize, 1) - 2*xTrain*xTest;
  
  #[M, I] = min(dist);
  #errorProd = yTrain(I).*yTest;
  
  [B, I] = sort(dist);
  
  errorProd = sum(yTrain(I(1:k, :)), 1) .* yTest;
  errorProd(errorProd<0) = -1;
  errorProd(errorProd>0) = 1;
  
  nErrors = -1 * sum(errorProd(errorProd<0));
  
end





