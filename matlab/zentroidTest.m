function nErrors = zentroidTest(xTest, yTest, mp, mn)
  [dim size] = size(xTest);
  nErrors = 0;
  for i=1:size
    if( (norm(xTest(:,i)-mn) - norm(xTest(:,i)-mp) ) * yTest(i) < 0)
      nErrors += 1;
    end
  end
end